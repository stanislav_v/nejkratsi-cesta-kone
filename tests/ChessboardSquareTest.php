<?php
    require __DIR__ . '/../src/class.ChessboardSquare.php';

    use PHPUnit\Framework\TestCase;

    final class ChessboardSquareTest extends TestCase
    {
        public function testCreateChessboardSquare(): void
        {
            $this -> assertInstanceOf(ChessboardSquare::class, new ChessboardSquare(2, 1));
        }

        /**
         * @dataProvider chessboardSquarePositionProvider
         */
        public function testCorrectCreation($col, $row, $string): void
        {
            $this -> assertEquals(''.(new ChessboardSquare($col, $row)), $string);
        }

        public function chessboardSquarePositionProvider(): array
        {
            $source = [];
            for($col = 1; $col < 9; $col++)
            {
                for($row = 1; $row < 9; $row++)
                {
                    $source[] = [$col, $row, chr(ord('a')-1+$col) . $row];
                }
            }
            return $source;
        }

        public function testCannotCreateColumnOutOfRangeBig(): void
        {
            $this -> expectException(Exception::class);

            new ChessboardSquare(9, 1);
        }

        public function testCannotCreateColumnOutOfRangeLow(): void
        {
            $this -> expectException(Exception::class);

            new ChessboardSquare(0, 1);
        }

        public function testCannotCreateRowOutOfRangeBig(): void
        {
            $this -> expectException(Exception::class);

            new ChessboardSquare(8, 9);
        }

        public function testCannotCreateRowOutOfRangeLow(): void
        {
            $this -> expectException(Exception::class);

            new ChessboardSquare(5, 0);
        }

        public function testCannotCreateRowOutOfRangeBigFromLetter(): void
        {
            $this -> expectException(Exception::class);

            new ChessboardSquare('k', 3);
        }

        public function testEquals(): void
        {
            $this -> assertTrue((new ChessboardSquare(3, 3))->equals(new ChessboardSquare('c', 3)));
        }

        public function testNotEquals(): void
        {
            $this -> assertFalse((new ChessboardSquare(3, 3))->equals(new ChessboardSquare(5, 3)));
        }

        /**
         * @dataProvider pathsDataProvider
         */
        public function testIsInPaths(array $paths, ChessboardSquare $inPaths, ChessboardSquare $notInPaths): void
        {
            $this -> assertTrue($inPaths -> isInPaths($paths));
        }

        /**
         * @dataProvider pathsDataProvider
         */
        public function testIsNotInPaths(array $paths, ChessboardSquare $inPaths, ChessboardSquare $notInPaths): void
        {
            $this -> assertFalse($notInPaths -> isInPaths($paths));
        }

        public function pathsDataProvider(): array
        {

            return [
                [
                    [
                        [new ChessboardSquare(3, 3), new ChessboardSquare(2, 1), new ChessboardSquare(5, 3)],
                        [new ChessboardSquare(7, 5), new ChessboardSquare(5, 3), new ChessboardSquare(5, 1)]
                    ],
                    new ChessboardSquare(5, 3), // in paths
                    new ChessboardSquare(1, 7) // not in paths
                ],
                [
                    [
                        [new ChessboardSquare(8, 3), new ChessboardSquare(6, 2), new ChessboardSquare(1, 8), new ChessboardSquare(2, 6), new ChessboardSquare(7, 1)],
                        [new ChessboardSquare(3, 5), new ChessboardSquare(1, 1), new ChessboardSquare(1, 5)]
                    ],
                    new ChessboardSquare(1, 1), // in paths
                    new ChessboardSquare(8, 8) // not in paths
                ]
            ];
        }

        /**
         * @dataProvider horseMoveRandomDataProvider
         * @dataProvider horseMoveFullDataProvider
         * @dataProvider horseMoveFullDataProviderAlternative
         */
        public function testHorseMovesCount(ChessboardSquare $square, int $availableMovesCount): void
        {
            $this -> assertSame(count($square -> getHorseMoves()), $availableMovesCount);
        }

        public function horseMoveRandomDataProvider(): array
        {
            return [
                [new ChessboardSquare(1, 1), 2],
                [new ChessboardSquare(1, 2), 3],
                [new ChessboardSquare(2, 2), 4],
                [new ChessboardSquare(8, 8), 2],
            ];
        }

        public function horseMoveFullDataProvider(): array
        {
            $source = [];
            for($col = 1; $col < 9; $col++)
            {
                for($row = 1; $row < 9; $row++)
                {
                    $moves = 8;
                    // there should not be spaces in the 'if' conditions,
                    // otherwise provider provides wrong data
                    if(($col>2)&&($col<7))
                    {
                        if (($row===2)||($row===7))
                        {
                            $moves -= 2;
                        }
                        if (($row===1)||($row===8))
                        {
                            $moves -= 4;
                        }
                    }
                    if(($col===2)||($col===7))
                    {
                        if(($row>2)&&($row<7))
                        {
                            $moves -= 2;
                        }
                        if(($row===2)||($row===7))
                        {
                            $moves -= 4;
                        }
                        if (($row===1)||($row===8))
                        {
                            $moves -= 5;
                        }
                    }
                    if(($col===1)||($col===8))
                    {
                        if(($row>2)&&($row<7))
                        {
                            $moves -= 4;
                        }
                        if(($row===2)||($row===7))
                        {
                            $moves -= 5;
                        }
                        if (($row===1)||($row===8))
                        {
                            $moves -= 6;
                        }
                    }
                    $source[] = [new ChessboardSquare($col, $row), $moves, $col, $row];
                }
            }
            return $source;
        }

        public function horseMoveFullDataProviderAlternative(): array
        {
            $map = [
                [0, 0, 0, 0, 0, 0, 0, 0, 0],
                [0, 2, 3, 4, 4, 4, 4, 3, 2],
                [0, 3, 4, 6, 6, 6, 6, 4, 3],
                [0, 4, 6, 8, 8, 8, 8, 6, 4],
                [0, 4, 6, 8, 8, 8, 8, 6, 4],
                [0, 4, 6, 8, 8, 8, 8, 6, 4],
                [0, 4, 6, 8, 8, 8, 8, 6, 4],
                [0, 3, 4, 6, 6, 6, 6, 4, 3],
                [0, 2, 3, 4, 4, 4, 4, 3, 2]
            ];
            $source = [];
            for($col = 1; $col < 9; $col++)
            {
                for($row = 1; $row < 9; $row++)
                {
                    $source[] = [new ChessboardSquare($col, $row), $map[$col][$row]];
                }
            }
            return $source;
        }
    }