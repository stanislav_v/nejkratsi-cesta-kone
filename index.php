<?php
	require_once 'src/function.findTheShortestWayOfTheHorse.php';

	const LOG_PROGRESS = false;



/*
// Examples:
najdiNejkratsiCestuKone(new ChessboardSquare(1,1), new ChessboardSquare(2,2));
najdiNejkratsiCestuKone(new ChessboardSquare(1,1), new ChessboardSquare(1,1));
najdiNejkratsiCestuKone(new ChessboardSquare(2,3), new ChessboardSquare(7,3));
najdiNejkratsiCestuKone(new ChessboardSquare(2,3), new ChessboardSquare(8,3));
najdiNejkratsiCestuKone(new ChessboardSquare(5,3), new ChessboardSquare(6,4));
najdiNejkratsiCestuKone(new ChessboardSquare(5,3), new ChessboardSquare(5,4));
najdiNejkratsiCestuKone(new ChessboardSquare(5,3), new ChessboardSquare(6,3));
najdiNejkratsiCestuKone(new ChessboardSquare(5,3), new ChessboardSquare(1,2));
najdiNejkratsiCestuKone(new ChessboardSquare('c',5), new ChessboardSquare('h',7));
*/

	$range = [1, 2, 3, 4, 5, 6, 7, 8];
	foreach($range as $sCol)
		foreach($range as $sRow)
			foreach($range as $dCol)
				foreach($range as $dRow)
					findTheShortestWayOfTheHorse(new ChessboardSquare($sCol,$sRow), new ChessboardSquare($dCol, $dRow));


	echo("\n" . 'OK' . "\n");

