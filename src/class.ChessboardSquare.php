<?php
    require_once 'function.printText.php';

    class ChessboardSquare {
        /**
         * Square row number 1 - 8
         * 
         * @var int
         */
        public int $row;

        /**
         * Square column number 1 - 8 corresponding to letters a - h
         * 
         * @var int
         */
        public int $col;

        /**
         * @param int|string    $col
         * @param int           $row
         * 
         * @throws Exception
         */
        public function __construct($col, int $row)
        {
            if (($row < 1) || ($row > 8)) {
                throw new Exception('Row number can be only in interval 1 - 8, ' . $row . ' received');
            }
            $this -> row = $row;

            if (is_int($col)) {
                if (($col < 1) || ($col > 8))
                    throw new Exception('Column number can be only in interval 1 - 8, ' . $col . ' received');

                $this -> col = $col;
            }
            elseif (is_string($col)) {
                if ((ord($col) < ord('a')) || (ord($col) > ord('h')))
                    throw new Exception('Column number can be only in interval a - h, ' . $col . ' received');
        
                $this -> col = ord($col) - ord('a') + 1;
            }
            else
                throw new Exception('Only string or int allowed, received ' . getType($col));
        }

        /**
         * @return string
         */
        public function __toString(): string
        {
            return chr(ord('a') - 1 + $this -> col) . $this -> row;
        }

        /**
         * Checks if the posistion of the other field is the same as the position of this field
         * 
         * @param ChessboardSquare $b
         * 
         * @return bool
         */
        public function equals(self $b): bool
        {
            return (($this -> row === $b -> row) && ($this -> col === $b -> col));
        }

        /**
         * Checks if this field exists in any of given paths
         * 
         * @param ChessboardSquare[][] $paths
         * 
         * @return bool
         */
        public function isInPaths(array $paths)
        {
            foreach($paths as $iPath => $path)
            {
                if(!$path[0] instanceof self)
                    continue;

                foreach($path as $field)
                    if($this -> equals($field))
                    {
                        printText($this . ' is in path ' . $iPath . ': ' . implode(' -> ', $path));
                        return true;
                    }
            }
            return false;
        }

        /**
         * Returns array of field to whose the horse can move from this field
         * 
         * @return ChessboardSquare[]
         */
        public function getHorseMoves()
        {
            $moveDefinition = [
                -1 => [-2, 2],
                1 => [-2, 2],
                -2 => [-1, 1],
                2 => [-1, 1]
            ];

            $availableMoves = [];

            foreach($moveDefinition as $col => $rows)
                foreach($rows as $row)
                {
                    $newCol = $this -> col + $col;
                    $newRow = $this -> row + $row;
                    if(($newCol >= 1) && ($newCol <= 8) && ($newRow >= 1) && ($newRow <= 8))
                        $availableMoves[] = new static($newCol, $newRow);
                }

            return $availableMoves;
        }
    }