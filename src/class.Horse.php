<?php
    require_once 'class.ChessboardSquare.php';
    require_once 'function.printText.php';

    class Horse
    {
        /**
         * Finds the shortest way of horse from the field $a to the field $b
         * 
         * @param ChessboardSquare $a The starting field
         * @param ChessboardSquare $b The destination field
         * 
         * @return ChessboardSquare[]   The array of fields visited by the horse on its way
         *                              from the starting field to the destination field
         *                              [] (empty field) - starting and destination fields are the same
         *                              [null] - path not found
         */
        public function shortestWayOfTheHorse(ChessboardSquare $a, ChessboardSquare $b): array
        {
            if ($a -> equals($b))
                return [];

            $paths = [[$a]];
            $iter = 0;

            // Nekonečná smyčka
            while (true)
            {
                $iter++;
                printText('Iterace: ' . $iter);
                $newPaths = [];
                foreach($paths as $index => $path)
                {
                    if(false === $path[0])
                    {
                        // cesty s příznakem 'dále nezpracovávat'
                        // (mají jako první položku FALSE)
                        // dále nezpracovávat
                        // $newPaths[] = $path;
                        continue;
                    }
                    printText('Cesta ' . $index . ': ' . implode(' -> ', $path));
                    $lastSquareInPath = end($path);
                    $moves = $lastSquareInPath -> getHorseMoves();
                    printText('Aktuální: ' . $lastSquareInPath . ', možné: ' . implode(', ', $moves));

                    foreach($moves as $move)
                    {
                        if($b -> equals($move))
                        {
                            // Cesta nalezena
                            $finalPath = $path;
                            $finalPath[] = $move;
                            printText('Cesta: ' . implode(' -> ', $finalPath));
                            return $finalPath;
                        }
                        $extendedPath = $path;
                        $extendedPath[] = $move;

                        // cesta zpět, dále nezpracovávat
                        $extendedPathLength = count($extendedPath);
                        if($extendedPathLength >= 3)
                            if($extendedPath[$extendedPathLength-1] -> equals($extendedPath[$extendedPathLength-3]))
                                array_unshift($extendedPath, false);

                        // pole už je v jiné cestě, není nutné pokračovat
                        // nalezená cesta by byla delší
                        if($move -> isInPaths($paths))
                            array_unshift($extendedPath, false);

                        $newPaths[] = $extendedPath;

                    }
                }
                $paths = $newPaths;

                if ($iter > 65) // Už jsou prošlá úplně všechna pole
                    break;
            }

            return [null];
        }
    }