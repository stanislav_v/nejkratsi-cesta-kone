<?php

    /**
     * Conditionally prints text into console
     * 
     * @param string $text
     */
    function printText(string $text)
    {

        if ((!defined('LOG_PROGRESS')) || (! LOG_PROGRESS))
            return;

        if(defined('STDOUT'))
            fwrite(STDOUT, $text . "\n");
    }