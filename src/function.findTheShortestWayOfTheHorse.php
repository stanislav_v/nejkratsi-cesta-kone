<?php
    require_once 'class.Horse.php';
    require_once 'class.ChessboardSquare.php';

    /**
     * Finds the shortest way of horse from the field $a to the field $b
     * and formats it into readable string
     * 
     * @param ChessboardSquare $a The starting field
     * @param ChessboardSquare $b The destination field
     * 
     * @return ChessboardSquare[]   The array of fields visited by the horse on its way
     *                              from the starting field to the destination field
     *                              [] (empty field) - starting and destination fields are the same
     *                              [null] - path not fount
     */
    function findTheShortestWayOfTheHorse(ChessboardSquare $a, ChessboardSquare $b)
    {
        $horse = new Horse;
        try
        {
            $path = $horse -> shortestWayOfTheHorse($a, $b);
            if (count($path) === 1)
                if (null === $path[0])
                    echo('Došlo k chybě, z ' . $a . ' do ' . $b . "\n");
            if(count($path) === 0)
                echo('Kůň už je na cílovém políčku, z ' . $a . ' do ' . $b . "\n");
    
            $strPath = implode(' -> ', $path);
            echo('Z ' . $a . ' do ' . $b . ': ' . $strPath . "\n");
    
        }
        catch(\Exception $e)
        {
            echo($e -> getMessage());
        }
    }